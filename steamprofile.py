import basics
import config
import json
import codecs
import sqlite3
from datetime import datetime
import requests
import copy as cp
import time
import progressbar
import os

class Game:
    ID = ''
    Playtime = 0.0
    Achievements = []
    
class Achievement:
    Name = ''
    Completed = False

class Friend:
    ID = 0
    Since = 0
    
class Profile:
    ProfileName = ''
    LastLogOff = ''
    URL = ''
    RealName = ''
    
    
def getGames():
    print("Starting gathering games.")
    Games = []
    params = {'key' : config.steamapikey, "steamid" : config.steamid64, 'format' : 'json'}
    result = basics.downloadString("http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?", params)
    result = json.loads(result)
    for game in result["response"]["games"]:
        newGame = Game()
        newGame.ID = game["appid"]
        newGame.Playtime = game["playtime_forever"]
        Games.append(newGame)
    print("Games gathering over.")
    return Games
    
def getAchievements(appId):
    Achievements = []
    params = {'appid' : appId, 'key' : config.steamapikey, 'steamid' : config.steamid64 }
    result = basics.downloadString("http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?", params)
    result = json.loads(result)
    success = result["playerstats"]["success"]
    if not success:
        return []
    try:
        for achievement in result["playerstats"]["achievements"]:
            newAchievement = Achievement()
            newAchievement.Name = achievement["apiname"]
            newAchievement.Completed = achievement["achieved"]
            Achievements.append(newAchievement)
        return Achievements
    except:
        return []

def getFriends():
    print("Starting gathering friends.")
    Friends = []
    params = {'key' : config.steamapikey, 'steamid' : config.steamid64 }
    result = basics.downloadString("http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?", params)
    result = json.loads(result)
    for friend in result["friendslist"]["friends"]:
        newFriend = Friend()
        newFriend.ID = friend["steamid"]
        newFriend.Since = friend["friend_since"]
        Friends.append(newFriend)
    print("Friends gathering over.")
    return Friends

def getProfile():
    profile = Profile()
    params = {'key' : config.steamapikey, 'steamids' : config.steamid64}
    result = basics.downloadString("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?", params)
    result = json.loads(result)["response"]["players"][0]
    profile.ProfileName = result["personaname"]
    profile.LastLogOff = result["lastlogoff"]
    profile.RealName = result["realname"]
    profile.URL = result["profileurl"]
    return profile
def main():
    config.steamid64
    Games = getGames()
    print("Starting gathering achievements.")
    counter = 0
    ProgressBar = None
    
    for game in Games:
        counter += 1
        label = "Loading " + str(game.ID) + " (" + str(counter) + "/" + str(len(Games)) + ")"
        if ProgressBar == None:
            ProgressBar = progressbar.ProgressBar(counter, len(Games), label = label, usePercentage = False)
        else:
            ProgressBar.updateProgress(counter, label)
        game.Achievements = getAchievements(game.ID)
        
    print("Achievements gathering over.")
    Friends = getFriends()
    profile = getProfile()
    
    if not os.path.exists(os.path.dirname(os.path.abspath(__file__)) + "/Steam"):
        os.makedirs(os.path.dirname(os.path.abspath(__file__)) + "/Steam")
    
    extension = "-" + datetime.now().strftime("%Y%m%d") + "-" + datetime.now().strftime("%H%M")

    db, cur = basics.openDatabase(os.path.dirname(os.path.abspath(__file__)) + "/Steam/Profile" + extension + ".s3db")
    db2, cur2 = basics.openDatabase(os.path.dirname(os.path.abspath(__file__)) + "/Steam/Profile.s3db")
    
    basics.createTable("Friends", ["SteamID", "Since"], ["INTEGER", "INTEGER"], cur)
    basics.createTable("Games", ["AppID", "Playtime"], ["INTEGER", "INTEGER"], cur)
    basics.createTable("Achievements", ["AppID", "Name", "Completed"], ["INTEGER", "TEXT", "INTEGER"], cur)
    
    basics.createTable("Profile", ["DATE", "ProfileName", "LastLogOff", "URL", "RealName"], ["TEXT", "TEXT", "INTEGER", "TEXT", "TEXT"], cur2)
    
    for game in Games:
        basics.addLine("Games", [game.ID, game.Playtime], cur)
        for achievement in game.Achievements:
            basics.addLine("Achievements", [game.ID, achievement.Name, achievement.Completed], cur)
    for friend in Friends:
        basics.addLine("Friends", [friend.ID, friend.Since], cur)
    
    
    basics.addLine("Profile", [datetime.now(), profile.ProfileName, profile.LastLogOff, profile.URL, profile.RealName], cur2)
    
    import badges
    badges.getBadges(db, cur)
    
    db2.commit()
    db.commit() 
    

if basics.verifyData():
    main()
else:
    print("Operation aborted.")
