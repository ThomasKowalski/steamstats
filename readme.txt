Installing
    install.sh => updates the files with the ones on BitBucket
    cron.sh => installs the cron script
    inventory => cron config file
Config file
    config.py => please complete this file with the relevant data
Python scripts
    inventory.py => gathers the Inventory of the SteamID
    steamprofile.py => gathers the profile data of the SteamID
Data files / folders
    Steam => data folder