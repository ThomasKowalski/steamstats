import config
import time

def writeToLogFile(message):
	ts = time.strftime("[%Y/%m/%d %H:%M]")
	with open(config.logfile, "a+") as f:
		f.write(ts + " " + message + "\n")
	return True

def resetLogFile():
	print("TODO")