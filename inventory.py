import basics
import config

import urllib.request
import urllib.parse
import json
import codecs
import sqlite3
from datetime import datetime
import requests
import copy as cp
import time
import os

class Item:
    Name = ''
    ClassID = ''
    InstanceID = ''
    IconURL = ''
    IconURLLarge = ''
    MarketHashName = ''
    Type = ''
    Tradable = False
    Marketable = False
    Wear = 0.0
    ID = 0
    Price = 0.0

    
#PARAMETRES
game = "730"
url = "http://steamcommunity.com/profiles/" + config.steamid64 + "/inventory/json/" + game + "/2"
#WEB

#Decodes the JSON of the inventory
def decodeJSON(string):
    Items = []
    result = json.loads(string)
    inventory = result["rgInventory"]
    descriptions = result["rgDescriptions"]
    for item in inventory.keys():
        content = inventory[item]
        newItem = Item()
        newItem.ID = content["id"] # = item
        newItem.ClassID = content["classid"]
        newItem.InstanceID = content["instanceid"]
        Items.append(newItem)
                
    for item in descriptions.keys():
        for item2 in Items:
            if str(item2.ClassID) + "_" + str(item2.InstanceID) == item:  
                item2.Name = descriptions[item]['name']
                item2.ClassID = descriptions[item]['classid']
                item2.InstanceID = descriptions[item]['instanceid']
                item2.Marketable = descriptions[item]['marketable']
                item2.Tradable = descriptions[item]['tradable']
                item2.Type = descriptions[item]['type']
                item2.MarketHashName = descriptions[item]['market_hash_name']
    getWear(Items)
    getPrice(Items)
    return Items

#Retrieves the prices of the items in the Item list    
def getPrice(Items):
    Prices = {}
    for item in Items:
        if item.Marketable == False:
            item.Price = -1
        else:
            if item.MarketHashName in list(Prices.keys()):
                item.Price = Prices[item.MarketHashName]
            else:
                params = {'currency' : '0', 'appid' : '730', 'market_hash_name':item.MarketHashName}
                string = basics.downloadString("http://steamcommunity.com/market/pricehistory/", params)
                result = json.loads(string)
                if (result["success"] == False):
                    print("Waiting")
                    # time.sleep(60)
                    # getPrice(Items)
                else:
                    prices = result["prices"]
                    counter = 0
                    for day in prices:
                        if counter < len(prices) - 1:
                            counter += 1
                        else:
                            item.Price = day[1]
                            Prices[item.MarketHashName] = day[1]
def getWear(Items):
    params = {'key' : config.steamapikey, "SteamID" : config.steamid64}
    result = basics.downloadString("http://api.steampowered.com/IEconItems_730/GetPlayerItems/v0001/", params)
    while ' '.join(result.split()) == "{ }":
        result = basics.downloadString("http://api.steampowered.com/IEconItems_730/GetPlayerItems/v0001/", params)
    for resultItem in json.loads(result)["result"]["items"]:
        ID = resultItem["id"]
        Wear = 0.0
        for attribute in resultItem["attributes"]:
            if attribute["defindex"] == 8:
                for item in Items:
                    if str(item.ID) == str(ID):
                        item.Wear = attribute["float_value"]

def main():
    global url
    tableName = "Inventory-" + datetime.now().strftime("%Y%m%d") + "-" + datetime.now().strftime("%H%M") #Set the table name before downloading the inventory so we get nice hours (not 12:01...)
    
    Items = decodeJSON(basics.downloadString(url, []))
    
    if not os.path.exists(os.path.dirname(os.path.abspath(__file__)) + "/Steam"):
        os.makedirs(os.path.dirname(os.path.abspath(__file__)) + "/Steam")        
    
    db, cur = basics.openDatabase(os.path.dirname(os.path.abspath(__file__)) + "/Steam/inventory.s3db")
    
    
    basics.createTable(tableName, ["ID", "Name", "ClassID", "InstanceID", "MarketHashName", "Type", "Tradable", "Marketable", "Wear", "Price"], ["INTEGER", "TEXT", "INTEGER", "INTEGER", "TEXT", "TEXT", "INTEGER", "INTEGER", "REAL", "REAL"], cur)
    
    db.commit()
    
    for item in Items:
        basics.addLine(tableName, [item.ID, item.Name, item.ClassID, item.InstanceID, item.MarketHashName, item.Type, item.Tradable, item.Marketable, item.Wear, item.Price], cur)

    db.commit()

if basics.verifyData():
    main()
else:
    print("Operation aborted.")
