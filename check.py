import basics
import config

import urllib.request
import urllib.parse
import json
import codecs
import sqlite3
from datetime import datetime
import requests
import copy as cp
import time
import os
import sys

def main():
    if basics.verifyData():
        params = {"steamids" : config.steamid64, "key" : config.steamapikey}
        result = basics.downloadString("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?", params)
        result = json.loads(result)
        try:
            if len(result["response"]["players"]) == 0:
                print("Wrong Steam ID. Aborting")
                sys.exit()
            else:
                print("Steam ID is correct.")
            state = result["response"]["players"][0]["communityvisibilitystate"]
            
            print("Checking privacy state of the account.")
            if state == 3:
                print("Account is public.")
            else:
                print("Account is not public. Aborting")
                sys.exit()
            print("Steam Web API Key is correct.")
        except:
            print("Steam Web API Key is incorrect. Aborting.")
        params = {'currency' : '0', 'appid' : '730', 'market_hash_name':'AWP | Lightning Strike (Factory New)'}
        result = basics.downloadString("http://steamcommunity.com/market/pricehistory/", params)
        if result == "[]":
            print("Steam login cookie is invalid. Aborting")
            sys.exit()
        else:
            print("Steam login cookie is correct.")
        print("Everything is OK! You can use the scripts!")
    else:
        print("Operation aborted.")

main()