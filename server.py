from os import listdir
from os.path import isfile, join
import os.path
from flask import Flask, render_template
import sqlite3
import datetime
import jinja2


import basics as basics


class Item:
    Name = ''
    ClassID = ''
    InstanceID = ''
    IconURL = ''
    IconURLLarge = ''
    MarketHashName = ''
    Type = ''
    Tradable = False
    Marketable = False
    Wear = 0.0
    ID = 0
    Price = 0.0
    
class Inventory:
    DBName = ""
    Date = datetime.datetime.now()
    Items = []

#Inventory Info

def DBNameToDate(name):
    return datetime.datetime.strptime(name.replace("Inventory-", ""), "%Y%m%d-%H%M")

def getTables(database):
    dbInv, curInv = basics.openDatabase(database)
    curInv.execute("SELECT name FROM sqlite_master WHERE type='table';")
    Tables  = curInv.fetchall()
    return Tables, dbInv, curInv
    
def getPath():
    return os.path.dirname(os.path.abspath(__file__))
   
def getData(cur, table):
    cur.execute("SELECT * FROM '" + table + "'")
    return cur.fetchall()

def getInventorySnapshots():
    Tables, db, cur = getTables(getPath() + "/Steam/inventory.s3db") #get the tables

    Snapshots = []

    for table in Tables:
        Rows = getData(cur, table[0])
        newInventory = Inventory()
        newInventory.DBName = table[0]
        newInventory.Date = DBNameToDate(table[0])
        for Row in Rows:
            newItem = Item()
            newItem.ID = Row[0]
            newItem.Name = Row[1]
            newItem.ClassID = Row[2]
            newItem.InstanceID = Row[3]
            newItem.MarketHashName = Row[4]
            newItem.Type = Row[5]
            newItem.Tradable = Row[6]
            newItem.Marketable = Row[7]
            newItem.Wear = Row[8]
            newItem.Price = Row[9]
            newInventory.Items.append(newItem)
        
        Snapshots.append(newInventory)
    return Snapshots
    
def getLastInventoryUpdate(): #laissons satan en paix
    if int(table[0].replace("Inventory-", "").replace("-", "")) > maxInv:
        lastInv = table[0]
        maxInv = int(table[0].replace("Inventory-", "").replace("-", ""))
    return -1        

def getFiles(pathToAnalyze):
    onlyfiles = [f for f in listdir(pathToAnalyze) if isfile(join(pathToAnalyze, f))]
    return onlyfiles
   
app = Flask(__name__)
@app.route("/")
def index():
    lastFile = ""
    max = 0
    for file in onlyfiles:
        if "Profile-" in file:
            if int(file.replace("Profile-", "").replace(".s3db", "").replace("-", "")) > max:
                lastFile = file
                max = int(file.replace("Profile-", "").replace(".s3db", "").replace("-", ""))

    return render_template("index.html", lastProfileUpdate = lastFile.replace("Profile-", ""), lastInventoryUpdate = getLastInventoryUpdate())

@app.route('/inventory')
def inventoryIndex():
    Available = ""
    for snap in getInventorySnapshots():
        Available += "<a href='/inventory/" + snap.DBName + "'>" + str(snap.Date) + "</a><br />"
    return render_template("inventoryindex.html", AvailableSnapshots = jinja2.Markup(Available))
    

@app.route('/inventory/<invdate>/')
def inventory(invdate):
    if invdate == "":
        return inventoryIndex()
        
    snapshots = getInventorySnapshots()
        
    for snap in snapshots:
        if snap.DBName == invdate:
            selectedSnapshot = snap
    
    print(str([len(snap.Items) for snap in snapshots]))
    
    totalValue = 0.0
    for item in snap.Items:
        totalValue += item.Price
    
    return render_template("inventory.html", Date=str(selectedSnapshot.Date), ItemsCount = len(selectedSnapshot.Items), TotalValue = totalValue)
    
if (__name__ == '__main__'):
    app.run(debug=True, host='0.0.0.0')
