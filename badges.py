import bs4 as BeautifulSoup
from datetime import datetime
import calendar

from basics import *
import config

debug = False


def getBadges(db, cur):
	finished = False
	while (not finished):
		print("New iteration.")
		try:
			webPage = downloadString("http://steamcommunity.com/profiles/" + config.steamid64 + "/badges/", {})
			soup = BeautifulSoup.BeautifulSoup(webPage, "html.parser")
			badgesSheet = soup.find("div", class_="badges_sheet")

			games = badgesSheet.findAll("div", recursive = False, class_ = "badge_row is_link")
			# , _class = "badge_row is_link"
			# badgesSheetBS = BeautifulSoup.BeautifulSoup(str(badgesSheet), "html.parser")

			# games = badgesSheetBS.findAll("div")

			for game in games:
				badgeTitle = game.find("div", class_ = "badge_title").getText().replace("View details", "").strip()
				# print(str([str(child) for child in game.find("div", class_ = "badge_info_image").children]))
				badgeImage = game.find("div", class_ = "badge_info_image").find("img").attrs["src"]  #this doesn't work.
				badgeName = game.find("div", class_ = "badge_info_title").getText().strip()
				badgeDateString = game.find("div", class_ = "badge_info_unlocked").getText().strip()
				
				badgeDateSplit = badgeDateString.split(" ")
				
				if "," in badgeDateString: #not this year:
					offset = 1
					year = int(badgeDateString.split(",")[1].split("@")[0].strip())
				else:
					offset = 0
					now = datetime.now()
					year = now.year
				
				months = ["First element lol", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
				
				day = int(badgeDateSplit[1])
				month = int(months.index(badgeDateSplit[2].strip(",")))
				
				rawTime = badgeDateSplit[4 + offset]
				timeSplit = rawTime.split(":")
				
				hour = int(timeSplit[0].strip())
				minute = int(timeSplit[1][:2].strip())
				
				if (timeSplit[1][2:5] == "pm"):
					hour += 12
				
				d = datetime(year, month, day, hour, minute)
				timestamp1 = calendar.timegm(d.timetuple())

				createTable("Badges", ["GameName", "BadgeName", "BadgeDate", "BadgeImage"], ["TEXT", "TEXT", "TEXT", "TEXT"], cur)
				
				addLine("Badges", [badgeTitle, badgeName, timestamp1, badgeImage], cur)
				
			finished = True
		except:
			pass
		
if debug:
	extension = "-" + datetime.now().strftime("%Y%m%d") + "-" + datetime.now().strftime("%H%M")
	db, cur = openDatabase(os.path.dirname(os.path.abspath(__file__)) + "/Steam/Profile" + extension + ".s3db")
	getBadges(db, cur)