echo "SteamStats setup / update script"
echo "Deleting old stuff"
#Remove any file that is in the directory and not config.py
find . -maxdepth 1 -type f ! -name config.py -delete

if [ -d "templates" ]
then
    rm "templates" -r
fi

#Remove HEAD.zip if it exists, in order not to get HEAD.zip.1...
if [ -f "HEAD.zip" ]
then
    rm HEAD.zip
fi

echo "Downloading the last version"
#Get the archive and extract it
wget "https://bitbucket.org/ThomasKowalski/steamstats/get/HEAD.zip"
echo "Extracting archive"
unzip HEAD.zip -d .
find ./ThomasKowalski*/ -mindepth 1 -type d -exec mv {} ./ \;
#Keep the old config.py
if [ -f config.py ]
then
    find ./ThomasKowalski*/ -mindepth 1 -maxdepth 1 -type f ! -name "config.py" -exec mv {} . \;
else
    find ./ThomasKowalski*/ -mindepth 1 -maxdepth 1 -type f -exec mv {} . \;
fi

echo "Removing temporary files"
#Remove the temporary file and folder
rm HEAD.zip
rm -r ThomasKowalski*/

echo "Converting shell scripts to the UNIX format"
#Convert the files to the UNIX format
dos2unix cron.sh
dos2unix update.sh

echo "Adding cron jobs"
#Add the cron jobs
sh cron.sh

echo "Done!"
