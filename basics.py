import config
import json
import codecs
import sqlite3
from datetime import datetime
import requests
import copy as cp
import time
import os


def verifyData():
    if config.steamid64 == "":
        print("No SteamID64 found in the config file. Please verify it's present and restart.")
        return False
    elif config.steamapikey == "":
        print("No Steam Web API Key found in the config file. Please enter it and restart")
        return False
    elif config.steamcookie == "":
        print("No Steam cookie value found in the config file. Please enter it and restart.")
        return False
    if config.csgoexchangebaseurl == "":
        print("Warning: No CSGO.exchange history URL provided. We won't be able to get your old data back.")
    return True
    
#BDD
def openDatabase(path):
    db = sqlite3.connect(path)
    cur = db.cursor()
    return (db, cur)

def createTable(name, fields, types, cur):
    sql = "CREATE TABLE '" + name + "' ("
    for i in range(len(fields)):
        sql += fields[i] + " " + types[i] + ","
    sql = sql.strip(",")
    sql = sql + ")"
    try:
        cur.execute(sql)
    except:
        pass
    
def addLine(table, content, cur):
    contentAsString = ""
    for elt in content:
        contentAsString += "?,"
    contentAsString = contentAsString.strip(",")
    sql = "INSERT INTO '" + table + "' VALUES (" + contentAsString + ")"
    cur.execute(sql, content)
#FIN BDD

def downloadString(url, params):
    cookie = {'steamLogin': config.steamcookie}
    params = params #useless
    data = requests.get(url,cookies=cookie,params=params)
    return data.text
