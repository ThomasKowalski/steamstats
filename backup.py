import sys
from config import *
from shutil import copyfile
import log
import os 

log.writeToLogFile("Backup started.")
try:
	if not os.path.exists(backupdir):
		os.makedirs(backupdir)
	copyfile("Steam/Profile.s3db", backupdir + "/" + "Profile.s3db")
	copyfile("Steam/inventory.s3db", backupdir + "/" + "inventory.s3db")
	copyfile("Steam/inventory_history.s3db", backupdir + "/" + "inventory_history.s3db")
	for file in os.listdir("./Steam"):
		if os.path.exists(backupdir + "/" + file) == False:
			copyfile("./Steam/" + file, backupdir + "/" + file)
			log.writeToLogFile("Adding " + file)
	log.writeToLogFile("Backup completed.")
except:
	log.writeToLogFile("Exception occured during backup." + str(sys.exc_info()[0]))