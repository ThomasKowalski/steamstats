import basics as basics
import config as config

import bs4 as BeautifulSoup
import urllib.request
import urllib.parse
import json
import codecs
import sqlite3
from datetime import datetime
import requests
from collections import namedtuple
import copy as cp
import time
import os

class Item:
    Name = ''
    ClassID = ''
    InstanceID = ''
    IconURL = ''
    IconURLLarge = ''
    MarketHashName = ''
    Type = ''
    Tradable = False
    Marketable = False
    Wear = 0.0
    ID = 0
    Price = 0.0


def downloadString(url, params):
    cookie = {'steamLogin': config.steamcookie}
    params = params #useless
    data = requests.get(url,cookies=cookie,params=params)
    return data.text
    

    
def main():
    data = downloadString(config.csgoexchangebaseurl, {})
    
    soup = BeautifulSoup.BeautifulSoup(data, "html.parser")
    
    Dates = []
    
    selector = soup.find("select", id="selHistory")
    for option in selector.contents:
        try:
            splitOption = str(option).replace("selected=\"\"", "").split("\"") #I know this is bad
            Dates.append(splitOption[1])
        except:
            pass
    
    Snapshots = {}
    for date in Dates:
        Items = []
        webPage = downloadString("http://csgo.exchange/id/" + config.steamid64 + "/history/" + date, {})
        soup = BeautifulSoup.BeautifulSoup(webPage, "html.parser")
        div = soup.find("div", class_="contentItems")
        
        div = BeautifulSoup.BeautifulSoup(str(div), "html.parser")
        for item in div.find_all(True):
            # for at in item.attrs.keys():
                # print(at)
            try:
                if "vItem" in item.attrs["class"]:
                    newItem = Item()
                    newItem.Wear = item.attrs["data-rawext"]
                    newItem.ID = item.attrs["data-id"]
                    newItem.Price = item.attrs["data-market"]
                    newItem.Name = item.attrs["data-hashname"]
                    Items.append(newItem)
            except:
                pass
        Snapshots[date] = Items
    
    for date in Snapshots.keys():
        if not os.path.exists(os.path.dirname(os.path.abspath(__file__)) + "/Steam"):
            os.makedirs(os.path.dirname(os.path.abspath(__file__)) + "/Steam")        
        
        db, cur = basics.openDatabase(os.path.dirname(os.path.abspath(__file__)) + "/Steam/inventory.s3db")
        
        tableName = "Inventory-" + datetime.fromtimestamp(int(date)).strftime("%Y%m%d-%H%M")
        
        basics.createTable(tableName, ["ID", "Name", "ClassID", "InstanceID", "MarketHashName", "Type", "Tradable", "Marketable", "Wear", "Price"], ["INTEGER", "TEXT", "INTEGER", "INTEGER", "TEXT", "TEXT", "INTEGER", "INTEGER", "REAL", "REAL"], cur)
        
        db.commit()
        
        for item in Snapshots[date]:
            basics.addLine(tableName, [item.ID, item.Name, item.ClassID, item.InstanceID, item.MarketHashName, item.Type, item.Tradable, item.Marketable, item.Wear, item.Price], cur)

        db.commit()      
        
main()